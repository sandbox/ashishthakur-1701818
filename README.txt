
-- SUMMARY --

The Rules Plus module provides a cool set of new events and actions which will
make life easier for you :).
New events added
  * Decrease in user login frequency.
  * Change in IP Address.
  * First time user login.
  * Failed login.


-- REQUIREMENTS --

* Rules: http://drupal.org/project/rules


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


--- DCD Demo --
This is a dummy readme
