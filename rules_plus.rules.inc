<?php

/**
 * @file
 * Rules Plus code: events, actions.
 */

/**
 * Implements hook_rules_event_info().
 */
function rules_plus_rules_event_info() {
  $items = array(
    'first_time_login' => array(
      'label' => t('First time user login'),
      'group' => t('Rules Plus'),
      'variables' => array(
        'user_object' => array(
          'type' => 'user',
          'label' => t('The user object'),
        ),
      ),
    ),
    'failed_user_login' => array(
      'label' => t('Failed user login'),
      'group' => t('Rules Plus'),
      'variables' => array(
        'user_object' => array(
          'type' => 'user',
          'label' => t('The user object'),
        ),
      ),
    ),
    'decrease_in_user_login_frequency' => array(
      'label' => t('Decrease in user login frequency'),
      'group' => t('Rules Plus'),
      'variables' => array(
        'user_object' => array(
          'type' => 'user',
          'label' => t('The user object'),
        ),
      ),
    ),
    'change_in_ip_address' => array(
      'label' => t('Change in IP Address of user'),
      'group' => t('Rules Plus'),
      'variables' => array(
        'user_object' => array(
          'type' => 'user',
          'label' => t('The user object'),
        ),
      ),
    ),
    'increase_in_content_comment_count' => array(
      'label' => t('Content receives a lot of comments'),
      'group' => t('Rules Plus'),
      'variables' => array(
        'node_object' => array(
          'type' => 'node',    
          'label' => t('The node object'),
        ),
      ),
    ),
    'increase_in_content_access_traffic' => array(
      'label' => t('Content receives a lots of traffic'),
      'group' => t('Rules Plus'),
      'variables' => array(
        'node_object' => array(
          'type' => 'node',
          'label' => t('The node object'),
        ),
      ),
    ),
  );
  return $items;
}
