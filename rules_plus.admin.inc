<?php

/**
 * @file
 * Administrative page callbacks for the rules plus module.
 */

/**
 * Form builder. Configure annotations.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function rules_plus_admin_settings() {
  $form['rules_plus']['rules_plus_ip_address'] = array(
    '#type' => 'checkbox',
    '#title' => t('Track IP addresses.'),
    '#description' => 'Check to track IP addresses. This can be a privacy issue!.',
    '#default_value' => variable_get('rules_plus_ip_address', TRUE),
  );
  $form['#submit'][] = 'rules_plus_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Submit function for user track admin settings.
 */
function rules_plus_admin_settings_submit($form, &$form_state) {
  $track_ips = $form_state['values']['rules_plus_ip_address'];
  variable_set('rules_plus_ip_address', $track_ips);
}
